SELECT journals.input_text, authors.email, journals.created_at
FROM authors
JOIN journals
ON authors.id = journals.author_id
ORDER BY journals.id