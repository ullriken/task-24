SELECT journals.input_text, authors.email, journals.created_at
FROM journals
INNER JOIN authors ON authors.id = journals.author_id;