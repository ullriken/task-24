Task 24 - Journal Database
We need a database to store journal entries
The following data needs to be stored
The author information - There could be multiple authors
Different journals � There can be journals for different events
A journal entry � The journal entry should have a tile, some text and a date the entry was made. The author should also be recorded and to which journal the entry was made
Tags for journal entries � Each journal entry can receive multiple tags � (�javascript�, �html�, �css� as examples)
First create an ERD (Entity Relationship Diagram)
Build your database in MySQL Workbench
Write the following
Write at least 3 test INSERT scripts for each table
Write at least 3 test UPDATE scripts for each table
Write at least 10 SELECT scripts that combine table data
Save ALL The SQL scripts that your write INCLUDING the Create Database and Create table scripts