CREATE TABLE `journals`
    (
	id INT(11) PRIMARY KEY AUTO_INCREMENT NOT NULL,
    author_id INT NOT NULL,
    FOREIGN KEY (author_id) REFERENCES authors(id),
	title VARCHAR(128),
    journal_event VARCHAR(128),
	input_text VARCHAR(255),
	created_at DATETIME NOT NULL DEFAULT NOW(),
	update_at DATETIME DEFAULT NULL
    )