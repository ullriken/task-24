SELECT authors.first_name, journals.journal_event, journals.created_at
FROM journals
INNER JOIN authors ON authors.id = journals.author_id;