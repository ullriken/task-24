SELECT authors.first_name, authors.last_name, journals.title, journals.created_at
FROM journals
INNER JOIN authors ON authors.id = journals.author_id;